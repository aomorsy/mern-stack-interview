import { useState } from "react";
import Annotation from "react-image-annotation";

/**
 * @package https://www.npmjs.com/package/react-image-annotation?activeTab=readme
 * @Task create a re-usable component which takes
 * 1. width and height of each image
 * 2. image src and alt
 * 3. initial img annotations
 * @Note all images will have border radius, margin with 5px from all sides
 *  
 */
function App() {
  const [annotation, setAnnotation] = useState({});
  const [annotations, setAnnotations] = useState([]);
  const changeAnnotation = (annotation) => {
    setAnnotation(annotation);
  };
  const submitAnnotation = (annotation) => {
    const { geometry, data } = annotation;
    setAnnotations((prev) => {
      return [...prev, { geometry, data: { ...data, id: Math.random() } }];
    });
  };

  return (
    <div style={{ width: "900px" }}>
      <Annotation
        src="https://images.pexels.com/photos/2105199/pexels-photo-2105199.jpeg"
        alt="children"
        annotations={annotations}
        value={annotation}
        onChange={changeAnnotation}
        onSubmit={submitAnnotation}
      />
    </div>
  );
}

export default App;
